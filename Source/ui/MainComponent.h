/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Slider::Listener,
                        public Button::Listener,
                        public Thread,
                        public Label::Listener

{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& audio_);
    
    void resized() override;
    
    void sliderValueChanged (Slider* slider);
    
    void run() override;
    
    void buttonClicked (Button*) override;

    /** Destructor */
    ~MainComponent();

    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    void labelTextChanged (Label* labelThatHasChanged) override;
    
private:
    Audio& audio;
    Slider gainSlider,freqSlider;
    TextButton CounterButton;
    Label CounterButtonLabel, TimerLabel;
    String onOff = "on", timerLabelText;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
